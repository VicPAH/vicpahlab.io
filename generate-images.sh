#!/bin/sh

set -e

function create_size {
    src_path="$1"
    scale="$2"

    dir_path="$(dirname "$src_path")"
    file_name="$(basename "$src_path")"

    file_noext="$(echo $file_name | awk '{a=index($0, "."); print substr($0, 0, a - 1)}')"
    file_ext="$(echo $file_name | awk '{a=index($0, "."); print substr($0, a + 1)}')"

    out_path="$dir_path/${file_noext}_${scale}.${file_ext}"

    echo "Generating $out_path"
    gm convert -size "$scale" "$src_path" -resize "$scale" "$out_path"
}

for xsize in $(seq 500 500 2000); do
    create_size src/img/_MG_2247.jpg "${xsize}x"
done
