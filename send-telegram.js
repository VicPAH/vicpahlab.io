const fs = require('fs')
const http = require('http')
const https = require('https')
const moment = require('moment-timezone')
const path = require('path')
const process = require('process')

const cli = require('cli')
const _ = require('lodash')
const TelegramBot = require('node-telegram-bot-api')

const entities = new (require('html-entities').AllHtmlEntities)()


const EVENT_PREFIX = String.fromCodePoint(0x1F5D3)
const NEWS_PREFIX = String.fromCodePoint(0x1F4F0)


function getCurrentSummaries(absUrl) {
  return new Promise((resolve, reject) => {
    const httpModule = absUrl.startsWith('https') ? https : http
    httpModule.get(absUrl, resp => {
      let data = ''
      resp.on('data', chunk => data += chunk)
      resp.on('end', () => {
        resolve(JSON.parse(data))
      })
    })
      .on('error', err => reject(err))
  })
}
function getNewSummaries(absPath) {
  return new Promise((resolve, reject) => {
    fs.readFile(absPath, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })
}


const buildForEachNew = opts => async (subPath, onNew, onChanged) => {
  const { rootPath, rootUrl } = opts

  const absPath = path.join(rootPath, subPath)
  const absUrl = `${rootUrl}${subPath}`

  const [curSummaries, newSummaries] = await Promise.all([
    getCurrentSummaries(absUrl),
    getNewSummaries(absPath),
  ])

  await Promise.all(Object.entries(newSummaries).map(([key, data]) => {
    if (!data.sendTelegram) {
      return null
    }

    if (
      onNew
        && !curSummaries[key]  // new item
        || !curSummaries[key].sendTelegram  // previously unsent
    ) {
      return onNew(data)
    } else if (
      onChanged
        && !_.isEqual(
          _.omit(curSummaries[key], ['sendTelegram']),
          _.omit(newSummaries[key], ['sendTelegram']),
        )
    ) {
      return onChanged(data)
    }

    return null
  }))
}


function parseCli() {
  const fields = {
    token: ['t', 'Telegram bot token', 'string'],
    chatId: ['c', 'Telegram chat ID to post to', 'string'],
    rootUrl: ['u', 'Root URL for the VicPAH site', 'url'],
    rootPath: ['p', 'Root path for the built VicPAH site', 'path'],
  }
  const opts = cli.parse(fields)
  for (const name of Object.keys(fields)) {
    if (!opts[name]) {
      console.error('Required argument missing:', name)
      process.exit(1)
    }
  }
  return opts
}


(async () => {
  const opts = parseCli()

  const bot = new TelegramBot(opts.token)
  const forEachNew = buildForEachNew(opts)
  try {
    await forEachNew('/news/index.json', async data => {
      console.log('Sending message for', data.permalink)
      return bot.sendMessage(
        opts.chatId,
        `${NEWS_PREFIX} [${data.title}](${data.permalink})\n\n${entities.decode(data.summary)}`,
        {parse_mode: 'Markdown'},
      )
    })
    const buildEventDataHandler = changed => async data => {
      console.log('Sending', changed ? 'change' : 'new', 'message for', data.permalink)
      const start = moment.tz(data.start, data.timezone || 'australia/melbourne')
      let startStr = start.format('MMMM Do YYYY, h:mma')
      if (start.tz() !== 'Australia/Melbourne') {
        startStr = `${startStr}, ${start.tz()}`
      }
      return bot.sendMessage(
        opts.chatId,
        `${EVENT_PREFIX} ${changed ? '*UPDATED:* ' : ''}[${data.title}](${data.permalink})\n_(${startStr})_\n\n${entities.decode(data.summary)}`,
        {parse_mode: 'Markdown'},
      )
    }
    await forEachNew(
      '/events/index.json',
      buildEventDataHandler(false),
      buildEventDataHandler(true),
    )
  } catch(err) {
    console.error(err)
  }
})()
