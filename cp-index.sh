#!/bin/bash
thisdir="$(cd "$(dirname "$0")"; pwd)"
src_dir="$thisdir/public/content"
build_dir="$thisdir/public"
src_dir_len=${#src_dir}

echo Copy from $src_dir to $build_dir

find "$src_dir" -iname 'index.md' -print0 |
    while IFS= read -r -d '' src_filename; do
        rel_filename="${src_filename:${src_dir_len}:2000}"
        rel_dirname="$(dirname "$rel_filename")"
        mkdir -p "${build_dir}${rel_dirname}"
        cp "${build_dir}/index.html" "${build_dir}${rel_dirname}"
    done
